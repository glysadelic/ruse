#!/bin/bash

TAG="latest"

echo "removing any running containers"
ruse_containers=$(docker ps -a|egrep 'ruse_server|svcrunner'|awk '{print $1}')
for container in $ruse_containers
do
            docker rm -f $container
done

echo "removing local docker images"
images=$(docker images | egrep 'glysade|none' | awk '{print $3}')
docker rmi $images

echo "downloading ruse image"
docker pull glysade/ruse:$TAG


