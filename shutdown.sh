#!/bin/bash

echo "docker stop ruse_server"
docker stop ruse_server

echo "docker stop svcrunner_default"
docker stop svcrunner_default

echo "docker stop svcrunner_fastrocs"
docker stop svcrunner_fastrocs

echo "docker stop svcrunner_sss"
docker stop svcrunner_sss

echo "docker stop svcrunner_rdkit_sss"
docker stop svcrunner_rdkit_sss
