#!/bin/bash

RUSE_BASE=/ruse/master python ruse/install/common.py
python scripts/init_shared_storage.py

lock_file="/home/centos/ruse/target/universal/stage/RUNNING_PID"
[[ -f $lock_file ]] && /bin/rm $lock_file

xmx=1024
xms=128
mem_file=/sys/fs/cgroup/memory/memory.limit_in_bytes
proc_mem_file=/proc/meminfo
if [[ -f $mem_file && -f $proc_mem_file ]]; then
    max_mem_bytes=$( cat $mem_file )
    max_mem=$(( max_mem_bytes/(1024*1024) ))
    proc_mem_kbytes=$( grep MemTotal $proc_mem_file | awk '{print $2}' )
    proc_mem=$(( proc_mem_kbytes/1024 ))
    [[ $proc_mem -lt $max_mem ]] && max_mem=$proc_mem
    xmx=$((max_mem*90/100))
    xms=$((max_mem*25/100))
    echo "setting Java xmx $xmx xms $xms"
fi
JAVA_OPTS="-server -Xmx${xmx}M -Xms${xms}M -agentlib:jdwp=transport=dt_socket,server=y,address=8000,suspend=n" /home/centos/ruse/target/universal/stage/bin/ruse

