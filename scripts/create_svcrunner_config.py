import argparse
import json
import multiprocessing
import os
import socket

import sys

if __name__ == '__main__':
    args = sys.argv[1:]

    # note the default for max-worker threads is the number of cores on the docker server
    # the number of cores allocated to a docker container is not discoverable within the container
    # unless specific cpus are assigned using --cpuset-cpus
    # see https://stackoverflow.com/questions/47545960/how-to-check-the-number-of-cores-used-by-docker-container

    parser = argparse.ArgumentParser(description='Customize svcrunner template')
    parser.add_argument('-a', '--ruse-address', type=str, default='localhost')
    parser.add_argument('-o', '--ruse-port', type=str, default='9000')
    parser.add_argument('-p', '--ruse-public-address', type=str)
    parser.add_argument('-e', '--entrez-email', type=str, default='xyz@zyz.com')
    parser.add_argument('-q', '--queue', type=str, default='default')
    parser.add_argument('-s', '--service-name', type=str)
    parser.add_argument('-r', '--resource-ids', type=int, nargs='+')
    parser.add_argument('-w', '--max_worker_threads', type=str, default=multiprocessing.cpu_count())
    parser.add_argument('-l', '--logpath', type=str, default="default")
    parser.add_argument('-b', '--pipeline_pilot', type=str)

    args = parser.parse_args()
    ruse_address = args.ruse_address
    ruse_public_address = args.ruse_public_address
    if not ruse_public_address:
        ruse_public_address = ruse_address
    queue = args.queue
    service_name = args.service_name
    resource_ids = args.resource_ids
    entrez_email = args.entrez_email
    max_worker_threads = args.max_worker_threads
    port = args.ruse_port
    pipeline_pilot_server = args.pipeline_pilot

    in_file = '/home/centos/scripts/svcrunner.linux.json'
    out_file = '/home/centos/svcrunner/conf/svcrunner.linux.json'

    with open(in_file, 'r') as in_fh, open(out_file, 'w') as out_fh:
        config = json.load(in_fh)
        for entry in config:
            if entry['name'] == 'ServerName':
                entry['value'] = ruse_address
            elif entry['name'] == 'ENTREZ_EMAIL':
                entry['value'] = entrez_email
            elif entry['name'] == 'PublicServerName':
                entry['value'] = ruse_public_address
            elif entry['name'] == 'MaxWorkerThreads':
                entry['value'] = max_worker_threads
            elif entry['name'] == 'PublicServerName':
                entry['value'] = 'ruse_public_address'
            elif entry['name'] == 'Queues':
                if queue == 'default':
                    entry['queues'] = ['default']
                else:
                    entry['queues'] = []
            elif entry['name'] == 'PersistServices':
                if service_name:
                    entry['persistServices'] = [{'service': service_name, 'queue': queue, 'resource_ids': resource_ids}]
                else:
                    entry['persistServices'] = []
            elif entry['name'] == 'Hostname':
                entry['value'] = socket.gethostname()
            elif entry['name'] == 'RunnerId':
                entry['value'] = 'runner_{}'.format(queue)
            elif entry['name'] == 'Port':
                entry['value'] = port
            elif entry['name'] == 'PipelinePilotServer':
                entry['value'] = pipeline_pilot_server

        json.dump(config, out_fh)

    #shutil.chown(out_file, user='centos')

    os.makedirs('/ruse/worker/files/tasks', exist_ok=True)
    os.makedirs('/ruse/worker/files/resources', exist_ok=True)
