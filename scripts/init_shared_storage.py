import os
import os.path
import shutil


def init_shared_storage():

    os.makedirs("/ruse/master/logs", exist_ok=True)
    os.makedirs("/ruse/master/dbase", exist_ok=True)

    os.makedirs("/ruse/master/files/tasks", exist_ok=True)
    os.makedirs("/ruse/master/files/resources", exist_ok=True)
    os.makedirs("/ruse/master/files/tmp", exist_ok=True)

    os.makedirs("/ruse/worker/files/tasks", exist_ok=True)
    os.makedirs("/ruse/worker/files/resources", exist_ok=True)
    for resource in range(1, 4):
        if not os.path.exists("/ruse/master/files/resources/{}".format(resource)):
            shutil.copytree("/home/centos/ruse/files/resources/{}/".format(resource), "/ruse/master/files/resources/{}".format(resource))
    os.makedirs("/ruse/worker/files/tmp", exist_ok=True)

    if not os.path.exists("/ruse/master/dbase/ruse.db"):
        shutil.copy("/home/centos/ruse/dbase/ruse.db", "/ruse/master/dbase/ruse.db")

if __name__ == '__main__':
    init_shared_storage()
