#!/bin/bash

rm /home/centos/svcrunner/conf/*.properties /home/centos/svcrunner/conf/*.json
/home/centos/anaconda/bin/python /home/centos/scripts/create_svcrunner_config.py "$@"
/home/centos/anaconda/bin/python /home/centos/scripts/create_log4j2.py "$@"

xmx=1024
xms=128
mem_file=/sys/fs/cgroup/memory/memory.limit_in_bytes
proc_mem_file=/proc/meminfo
if [[ -f ${mem_file} && -f ${proc_mem_file} ]]; then
    max_mem_bytes=$( cat ${mem_file} )
    max_mem=$(( max_mem_bytes/(1024*1024) ))
    proc_mem_kbytes=$( grep MemTotal ${proc_mem_file} | awk '{print $2}' )
    proc_mem=$(( proc_mem_kbytes/1024 ))
    [[ ${proc_mem} -lt ${max_mem} ]] && max_mem=${proc_mem}
    xmx=$((max_mem*60/100))
    xms=$((max_mem*25/100))
    echo "setting Java xmx $xmx xms $xms"
fi
#java -server -Xmx${xmx}M -Xms${xms}M  -agentlib:jdwp=transport=dt_socket,server=y,address=8000,suspend=n -jar /home/centos/svcrunner/target/svcrunner-1.0.0.jar
java -server -Xmx${xmx}M -Xms${xms}M -jar /home/centos/svcrunner/target/svcrunner-1.0.0.jar
