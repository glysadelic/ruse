// CDD 2.0 does not need this file to be referenced from the HTML
// Add this file to make it compatible to the CDD 1.x users
console.warn('File ChemdrawWebManager.js is not needed anymore.');
console.warn('File chemdrawweb.nocache.js is renamed to chemdrawweb.js.');
