window.workingIndicator = {};

workingIndicator.show = function (text) {
  $("#workingIndicatorText").html(text);
  $("#workingIndicator").fadeIn();
};

workingIndicator.hide = function () {
  $("#workingIndicator").fadeOut();
  $("#workingIndicatorText").html("");
};