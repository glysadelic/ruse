$("body").append("<div id='defaultCddContainer' style='display:block;'></div>");

window.cddApiSample = {};
var PERKINELMER_DOMAINS = ['scienceaccelerated.com', 'perkinelmer.cloud', 'perkinelmer.com'];
cddApiSample.apiLists = [];

cddApiSample.InputType = {
  NoInput: 0,
  ShortText: 1,
  LongText: 2,
  Boolean: 3,
  JsonObject: 4,
  ArrayNumber: 5,
  Size: 6,
  Structure: 7,
  Handler: 8,
  OptionalOptions: 9,
  Options: 10,
  NoArgumentsHandler: 11,
};

cddApiSample.OutputType = {
  NoOutput: 0,
  ReadonlyText: 1,
  CopiableText: 2,
  Image: 3,
  Boolean: 4,
  FragmentInfo: 5,
  Size: 6,
  Number: 7,
  ArrayString: 8
};

cddApiSample.CallType = {
  NoCall: 0, 
  Sync: 1, 
  Async: 2, 
  JavaScript: 3
};

cddApiSample.apiReadyCallback = function () { };

cddApiSample.cddReadyCallback = function (cddInstance) {
  window.defaultCddInstance = cddInstance;
  $("#defaultCddContainer").hide();
  if (cddApiSample.apiReadyCallback) {
    cddApiSample.apiReadyCallback();
  }
};

cddApiSample.newApi = function (categoryName, displayName, functionName, callType, inputType, outputType, requireActiveDoc, description, usage, executeFunc) {
  var api = {};
  api.categoryName = categoryName;
  api.displayName = displayName;
  api.functionName = functionName;
  api.callType = callType;
  api.inputType = inputType;
  api.outputType = outputType;
  api.requireActiveDoc = requireActiveDoc;
  api.description = description;
  api.usage = usage;
  api.executeFunc = executeFunc;

  if (!api.usage)
  {
    api.usage = "chemdraw." + functionName;
    if (callType == cddApiSample.CallType.Sync) {
      if (inputType == cddApiSample.InputType.ShortText || inputType == cddApiSample.InputType.LongText) {
        api.usage = api.usage + "(string);";
      } else if (inputType == cddApiSample.InputType.Boolean) {
        api.usage = api.usage + "(boolean);";
      } else if (inputType == cddApiSample.InputType.JsonObject) {
        api.usage = api.usage + "(string|jsonObject);";
      } else if (inputType == cddApiSample.InputType.ArrayNumber) {
        api.usage = api.usage + "(Array<number>);";
      } else if (inputType == cddApiSample.InputType.Size) {
        api.usage = api.usage + "(Size[, options])";
      } else if (inputType == cddApiSample.InputType.Structure) {
        api.usage = api.usage + "(content, mimeType, callback)";
      } else if (inputType == cddApiSample.InputType.Handler || inputType == cddApiSample.InputType.NoArgumentsHandler) {
        api.usage = api.usage + "(handler)";
      } else if (inputType == cddApiSample.InputType.OptionalOptions) {
        api.usage = api.usage + "([options])";
      } else if (inputType == cddApiSample.InputType.Options) {
        api.usage = api.usage + "(options)";
      } else {
        api.usage = api.usage + "();";
      }
    } else if (callType == cddApiSample.CallType.Async) {
      if (inputType == cddApiSample.InputType.ShortText || inputType == cddApiSample.InputType.LongText) {
        api.usage = api.usage + "(string, callback);";
      } else if (inputType == cddApiSample.InputType.JsonObject) {
        api.usage = api.usage + "(jsonObject, callback);";
      } else if (inputType == cddApiSample.InputType.Structure) {
        api.usage = api.usage + "(content, mimeType, callback);";
      } else if (inputType == cddApiSample.InputType.Options) {
        api.usage = api.usage + "(options)";
      } else {
        api.usage = api.usage + "(callback);";
      }
    }
  }

  if (outputType == cddApiSample.OutputType.ReadonlyText || outputType == cddApiSample.OutputType.CopiableText || outputType == cddApiSample.OutputType.Image) {
    api.outputTypeString = "string";
  } else if (outputType == cddApiSample.OutputType.Boolean) {
    api.outputTypeString = "boolean";
  } else if (outputType == cddApiSample.OutputType.FragmentInfo) {
    api.outputTypeString = "Array<FragmentInfo>";
  } else if (outputType == cddApiSample.OutputType.Size) {
    api.outputTypeString = "Size";
  } else if (outputType == cddApiSample.OutputType.Number) {
    api.outputTypeString = "number";
  } else if (outputType == cddApiSample.OutputType.ArrayString) {
    api.outputTypeString = "Array<string>";
  } else {
    api.outputTypeString = "void";
  }

  api.clone = function () {
    var newApi = {};

    newApi.categoryName = this.categoryName;
    newApi.displayName = this.displayName;
    newApi.functionName = this.functionName;
    newApi.callType = this.callType;
    newApi.inputType = this.inputType;
    newApi.outputType = this.outputType;
    newApi.outputTypeString = this.outputTypeString;
    newApi.requireActiveDoc = this.requireActiveDoc;
    newApi.description = this.description;
    newApi.usage = this.usage;
    newApi.executeFunc = this.executeFunc;

    return newApi;
  };

  return api;
};

cddApiSample.registerApi = function (categoryName, displayName, functionName, callType, inputType, outputType, requireActiveDoc, description, usage, executeFunc) {
  var existingCategorizedApis = {};
  for (var apiListId in cddApiSample.apiLists) {
    if (cddApiSample.apiLists[apiListId].category == categoryName) {
      existingCategorizedApis = cddApiSample.apiLists[apiListId];
      break;
    }
  }

  if (!existingCategorizedApis.category) {
    existingCategorizedApis.category = categoryName;
    existingCategorizedApis.apiList = [];
    cddApiSample.apiLists.push(existingCategorizedApis);
  }

  var api = cddApiSample.newApi(categoryName, displayName, functionName, callType, inputType, outputType, requireActiveDoc, description, usage, executeFunc);

  existingCategorizedApis.apiList.push(api);
};

cddApiSample.errorCallback = function (id, configObject, error) {
  $(id).html("");
  if (error.name === 'LicenseError') {
    // license error, let user input license again
    cddApiSample.licenseErrorHandler(id, configObject, error);
  } else {
    alert(error.message);
  }
};

cddApiSample.showMessageToPlaceLicense = function (message) {
  var modalLicense = $('#modalLicense');
  $("#modalLicenseContent").html(message);
  modalLicense.modal({ keyboard: false });
}

cddApiSample.licenseErrorHandler = function (id, configObject, error) {
  if (error.message === 'License is not valid') {
    $('#modalLicenseInputTitle').html('Invalid License');
    var message = 'To try the sample page, please provide a valid license file.';
    cddApiSample.showMessageToPlaceLicense(message);
  } else if (error.message === 'License has expired') {
    $('#modalLicenseInputTitle').html('Expired License');
    var message = 'Your license has expired. Please provide a new license file.';
    cddApiSample.showMessageToPlaceLicense(message);
  } else {
    $.get('./path.txt').done(function (installPath) {
      var doneMessage = 'To try the sample page, please copy "ChemDraw-JS-License.xml" to ' + installPath;
      cddApiSample.showMessageToPlaceLicense(doneMessage);
    }).fail(function () {
      var failedMessage = 'Unable to locate the license file. Please make sure ChemDraw JS service \
      is properly installed and copy "ChemDraw-JS-License.xml" to the same folder of this sample page.';
      cddApiSample.showMessageToPlaceLicense(failedMessage);
    });
  }
};

cddApiSample.initialize = function (callback) {
  cddApiSample.apiReadyCallback = callback;
  var configObject = {
    id: "defaultCddContainer",
    preservePageInfo: true,
    viewonly: true,
    licenseUrl: './ChemDraw-JS-License.xml',
    callback: cddApiSample.cddReadyCallback,
    errorCallback: function (error) {
      cddApiSample.errorCallback("#defaultCddContainer", configObject, error);
    }
  };
  
  perkinelmer.ChemdrawWebManager.attach(configObject);
  var currentUrl = location.hostname.split('.').slice(-2).join('.');
  var devGuideUrl = (PERKINELMER_DOMAINS.indexOf(currentUrl) >= 0) ? "window.open('../docs/Developers%20Guide_Service/Developers%20Guide.htm')" : "window.open('../docs/Developers%20Guide/Developers%20Guide.htm')";
  // Register the APIs to the menus

  // Document
  cddApiSample.registerApi("Document", "Clear", "clear", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.NoOutput, true, "Clears all drawings on the ChemDraw canvas. This operation is not undoable, and ChemDraw will not prompt the user to confirm the operation.");
  cddApiSample.registerApi("Document", "Is Blank", "isBlankStructure", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.Boolean, true, "Returns true if nothing has been drawn in ChemDraw and false if something has been drawn.");
  cddApiSample.registerApi("Document", "-", "#");
  cddApiSample.registerApi("Document", "Fit to Container", "fitToContainer", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.NoOutput, true, "Resizes ChemDraw instance to fit to the container.");
  cddApiSample.registerApi("Document", "Get Canvas Size", "getCanvasSize", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.Size, true, "Gets ChemDraw canvas size.");
  cddApiSample.registerApi("Document", "Set Canvas Size", "setCanvasSize", cddApiSample.CallType.Sync, cddApiSample.InputType.Size, cddApiSample.OutputType.NoOutput, true, "Sets ChemDraw canvas size.");
  cddApiSample.registerApi("Document", "-", "#");
  cddApiSample.registerApi("Document", "Get Document Name", "getDocumentName", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.CopiableText, true, "Gets the underlying document name which is saved in the CDXML.");
  cddApiSample.registerApi("Document", "Set Document Name", "setDocumentName", cddApiSample.CallType.Sync, cddApiSample.InputType.LongText, cddApiSample.OutputType.NoOutput, true, "Sets the document name to the resulting CDXML.");
  cddApiSample.registerApi("Document", "-", "#");
  cddApiSample.registerApi("Document", "Get Errors", "getErrors", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.ArrayString, true, "Returns an array of strings describing the chemical errors that are present in the current drawings.");
  cddApiSample.registerApi("Document", "Is Saved", "isSaved", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.Boolean, true, "Determines if there is any unsaved change.");
  cddApiSample.registerApi("Document", "Mark As Saved", "markAsSaved", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.NoOutput, true, "Sets the dirty flag to indicate the canvas is saved and clean.");
  cddApiSample.registerApi("Document", "Set Content Change Handler", "setContentChangedHandler", cddApiSample.CallType.Sync, cddApiSample.InputType.Handler, cddApiSample.OutputType.NoOutput, true, "Sets a callback function which will be invoked when user makes any change on the canvas.");
  cddApiSample.registerApi("Document", "Toggle User Interaction (Set ViewOnly)", "setViewOnly", cddApiSample.CallType.Sync, cddApiSample.InputType.Boolean, cddApiSample.OutputType.NoOutput, true, "Sets whether the user is able to interact with ChemDraw.");
  cddApiSample.registerApi("Document", "-", "#");
  cddApiSample.registerApi("Document", "Load Configuration", "loadConfig", cddApiSample.CallType.Sync, cddApiSample.InputType.JsonObject, cddApiSample.OutputType.NoOutput, true, "Applies a new configuration into ChemDraw.");
  cddApiSample.registerApi("Document", "Load Remote Configuration from URL", "loadConfigFromUrl", cddApiSample.CallType.Async, cddApiSample.InputType.ShortText, cddApiSample.OutputType.NoOutput, true, "Applies a new configuration from remote url into ChemDraw.");
  cddApiSample.registerApi("Document", "-", "#");
  cddApiSample.registerApi("Document", "Get Version", "getVersion", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.CopiableText, false, "Returns the current version of ChemDraw as a string.");
  cddApiSample.registerApi("Document", "-", "#");
  cddApiSample.registerApi("Document", "Get Available Command Names", "getAvailableCommandNames", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.ArrayString, true, "Gets all available commands on the document.");
  cddApiSample.registerApi("Document", "Is Command Enabled", "getCommandWithName", cddApiSample.CallType.Sync, cddApiSample.InputType.ShortText, cddApiSample.OutputType.Boolean, true, "Check if command is enabled", "chemdraw.getCommandWithName(string).isEnabled;", function(context, cmdName) { return context.getCommandWithName(cmdName).isEnabled;});
  cddApiSample.registerApi("Document", "Is Command Checked", "getCommandWithName", cddApiSample.CallType.Sync, cddApiSample.InputType.ShortText, cddApiSample.OutputType.Boolean, true, "Check if command is checked", "chemdraw.getCommandWithName(string).isChecked;", function(context, cmdName) { return context.getCommandWithName(cmdName).isChecked;});  
  cddApiSample.registerApi("Document", "Execute Command by Name", "getCommandWithName.execute" , cddApiSample.CallType.JavaScript, cddApiSample.InputType.ShortText, cddApiSample.OutputType.NoOutput, true, "Execute a specified command.", "chemdraw.getCommandWithName(string).execute();", function(context, cmdName) { return context.getCommandWithName(cmdName).execute();});

  //Selection
  cddApiSample.registerApi("Selection", "Is Empty", "selection.isEmpty", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.Boolean, true, "Returns true if nothing has been selected in ChemDraw and false if something has been selected.")
  cddApiSample.registerApi("Selection", "Set Change Handler", "selection.onChange", cddApiSample.CallType.Sync, cddApiSample.InputType.NoArgumentsHandler, cddApiSample.OutputType.NoOutput, true, "Sets a callback function which will be invoked when selection changed on the canvas.");
  cddApiSample.registerApi("Selection", "Get CDXML", "selection.getCDXML", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.CopiableText, true, "Returns a CDXML string representing the current selected drawing.");
  cddApiSample.registerApi("Selection", "Get Image", "selection.getImageUrl", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.Image, true, "Returns a PNG Base64 representing the current selected drawing.");
  cddApiSample.registerApi("Selection", "Get SVG", "selection.getSVG", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.Image, true, "Returns a SVG string representing the current selected drawing.");
  cddApiSample.registerApi("Selection", "Contains Partial Structure", "selection.containsPartialStructure", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.Boolean, true, "Returns true if part of a single molecule or grouped molecules have been selected");
  cddApiSample.registerApi("Selection", "Number of Structures", "selection.numberOfStructures", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.Number, true, "Returns the count of fully or partially selected molecules/grouped molecules.");
  cddApiSample.registerApi("Selection", "Number of Objects", "selection.numberOfObjects", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.Number, true, "Returns the count of fully or partially selected objects or grouped objects, including molecules, arrows and etc.");

  // Structure
  cddApiSample.registerApi("Structure", "Load B64CDX", "loadB64CDX", cddApiSample.CallType.Async, cddApiSample.InputType.LongText, cddApiSample.OutputType.NoOutput, true, "Reads a BASE64 encoded CDX string and loads its contents into ChemDraw. This call is asynchronous. A callback function can to be provided and it will be invoked after the loading is done.");
  cddApiSample.registerApi("Structure", "Load CDXML", "loadCDXML", cddApiSample.CallType.Sync, cddApiSample.InputType.LongText, cddApiSample.OutputType.NoOutput, true, "Reads a CDXML string and loads its contents into ChemDraw.");
  cddApiSample.registerApi("Structure", "Load MOL", "loadMOL", cddApiSample.CallType.Async, cddApiSample.InputType.LongText, cddApiSample.OutputType.NoOutput, true, "Reads a MOL string and loads its contents into ChemDraw. This call is asynchronous. A callback function can to be provided and it will be invoked after the loading is done.");
  cddApiSample.registerApi("Structure", "Load SMILES", "loadSMILES", cddApiSample.CallType.Async, cddApiSample.InputType.LongText, cddApiSample.OutputType.NoOutput, true, "Reads a SMILES string and loads its contents into ChemDraw. This call is asynchronous. A callback function can to be provided and it will be invoked after the loading is done.");
  cddApiSample.registerApi("Structure", "-", "#");
  cddApiSample.registerApi("Structure", "Get CDXML", "getCDXML", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.CopiableText, true, "Returns a CDXML string representing the current drawing.");
  cddApiSample.registerApi("Structure", "Get MOL", "getMOL", cddApiSample.CallType.Async, cddApiSample.InputType.NoInput, cddApiSample.OutputType.CopiableText, true, "Translates the current drawing into a MOL v2000 representation. This call is asynchronous. A callback function can to be provided and it will be invoked after the result is generated.");
  cddApiSample.registerApi("Structure", "Get MOL3000", "getMOLV3000", cddApiSample.CallType.Async, cddApiSample.InputType.NoInput, cddApiSample.OutputType.CopiableText, true, "Translates the current drawing into a MOL v3000 representation. This call is asynchronous. A callback function can to be provided and it will be invoked after the result is generated.");
  cddApiSample.registerApi("Structure", "Get SMILES", "getSMILES", cddApiSample.CallType.Async, cddApiSample.InputType.NoInput, cddApiSample.OutputType.CopiableText, true, "Translates the current drawing into a SMILES representation. This call is asynchronous. A callback function can to be provided and it will be invoked after the result is generated.");
  cddApiSample.registerApi("Structure", "Get RXN", "getRXN", cddApiSample.CallType.Async, cddApiSample.InputType.NoInput, cddApiSample.OutputType.CopiableText, true, "Translates the current drawing into a RXN v2000 representation. This call is asynchronous. A callback function can to be provided and it will be invoked after the result is generated.");
  cddApiSample.registerApi("Structure", "Get RXN V3000", "getRXNV3000", cddApiSample.CallType.Async, cddApiSample.InputType.NoInput, cddApiSample.OutputType.CopiableText, true, "Translates the current drawing into a RXN v3000 representation. This call is asynchronous. A callback function can to be provided and it will be invoked after the result is generated.");
  cddApiSample.registerApi("Structure", "Get InChI", "getInChI", cddApiSample.CallType.Async, cddApiSample.InputType.NoInput, cddApiSample.OutputType.CopiableText, true, "Translates the current drawing into an InChI representation. This call is asynchronous. A callback function can to be provided and it will be invoked after the result is generated.");
  cddApiSample.registerApi("Structure", "Get InChIKey", "getInChIKey", cddApiSample.CallType.Async, cddApiSample.InputType.NoInput, cddApiSample.OutputType.CopiableText, true, "Translates the current drawing into an InChIKey representation. This call is asynchronous. A callback function can to be provided and it will be invoked after the result is generated.");
  cddApiSample.registerApi("Structure", "Get B64CDX", "getB64CDX", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.CopiableText, true, "Returns a BASE64 encoded CDX string representing the current drawing.");
  cddApiSample.registerApi("Structure", "-", "#");
  cddApiSample.registerApi("Structure", "Get Properties", "getProperties", cddApiSample.CallType.Async, cddApiSample.InputType.NoInput, cddApiSample.OutputType.CopiableText, true, "Returns properties of the current drawing. This call is asynchronous. A callback function can to be provided and it will be invoked after the result is generated.");
  cddApiSample.registerApi("Structure", "-", "#");
  cddApiSample.registerApi("Structure", "Get Image", "getImgUrl", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.Image, true, "Returns a URL string which points to a PNG representation of the current drawing. The image contents are encoded in the URL so that it can be used offline.");
  cddApiSample.registerApi("Structure", "Get SVG", "getSVG", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.Image, true, "Return a SVG representation for the current drawing.");

  // Reaction
  cddApiSample.registerApi("Reaction", "Add Product", "addProduct", cddApiSample.CallType.Sync, cddApiSample.InputType.LongText, cddApiSample.OutputType.Number, true, "Appends a new product to the reaction present in the current drawing. If there are products present, the new product will be added to the right of the right-most product. If no reaction is present, a new arrow will be added to the center of the canvas along with the product.");
  cddApiSample.registerApi("Reaction", "Add Reactant", "addReactant", cddApiSample.CallType.Sync, cddApiSample.InputType.LongText, cddApiSample.OutputType.Number, true, "Appends a new reactant to the reaction present in the current drawing. If there are reactants present, the new reactant is added to the left of the left-most reactant. If no reaction is present, a new arrow is added to the center of the canvas along with the reactant.");
  cddApiSample.registerApi("Reaction", "Add Reagent", "addReagent", cddApiSample.CallType.Sync, cddApiSample.InputType.LongText, cddApiSample.OutputType.Number, true, "Appends a new reagent to the reaction present in the current drawing. The reagent is added to the top of the reaction arrow and above the top-most reagent if there are any already. If no reaction is present, a new arrow is added to the center of the canvas, along with the reagent.");
  cddApiSample.registerApi("Reaction", "-", "#");
  cddApiSample.registerApi("Reaction", "Find Reactions", "findReactions", cddApiSample.CallType.Async, cddApiSample.InputType.NoInput, cddApiSample.OutputType.CopiableText, true, "Returns reactions information of the current drawing. This call is asynchronous and a callback function must be provided that is informed when the results are available.");
  cddApiSample.registerApi("Reaction", "-", "#");
  cddApiSample.registerApi("Reaction", "Get All Fragments Info", "getAllFragmentsInfo", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.FragmentInfo, true, "Gets information of all fragments on the canvas which contains CDXML string and preview PNG picture.");
  cddApiSample.registerApi("Reaction", "Get Fragments Info", "getFragmentsInfo", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.FragmentInfo, true, "Gets fragments information which contains CDXML string and preview PNG picture.");
  cddApiSample.registerApi("Reaction", "Get Selected Fragments Info", "getSelectedFragmentsInfo", cddApiSample.CallType.Sync, cddApiSample.InputType.NoInput, cddApiSample.OutputType.FragmentInfo, true, "Gets information of the selected fragments on the canvas which contains CDXML string and preview PNG picture.");
  cddApiSample.registerApi("Reaction", "-", "#");
  cddApiSample.registerApi("Reaction", "Label Fragments", "labelFragments", cddApiSample.CallType.Sync, cddApiSample.InputType.ArrayNumber, cddApiSample.OutputType.NoOutput, true, "Auto-number the fragments of the reaction with Roman numeral labels. Any existing labels will be replaced with the new ones.");

  // Utilities
  cddApiSample.registerApi("Utilities", "Convert to CDXML", "convertToCDXML", cddApiSample.CallType.Async, cddApiSample.InputType.Structure, cddApiSample.OutputType.CopiableText, false, "Converts structure string to CDXML format.");
  cddApiSample.registerApi("Utilities", "Get Molecular Weight", "getMolecularWeight", cddApiSample.CallType.Async, cddApiSample.InputType.Structure, cddApiSample.OutputType.CopiableText, false, "Gets molecular weight of the structure.");
  cddApiSample.registerApi("Utilities", "Convert Name to CDXML", "nameToStructure", cddApiSample.CallType.Async, cddApiSample.InputType.ShortText, cddApiSample.OutputType.CopiableText, false, "Reads a structure name and returns a CDXML string representing the structure.");
  cddApiSample.registerApi("Utilities", "Convert CDXML to Name", "structureToName", cddApiSample.CallType.Async, cddApiSample.InputType.LongText, cddApiSample.OutputType.CopiableText, false, "Reads a CDXML string and returns the name of the structure.");

  // Help
  cddApiSample.registerApi("Help", "User Guide", "Open User Guide", cddApiSample.CallType.JavaScript, cddApiSample.InputType.NoInput, cddApiSample.OutputType.NoOutput, false, "Opens the User Guide.", "window.open('../docs/User%20Guide/ChemDraw%20JS.htm');", function(){ return window.open('../docs/User%20Guide/ChemDraw%20JS.htm'); });
  cddApiSample.registerApi("Help", "Developer's Guide", "Open Developer's Guide", cddApiSample.CallType.JavaScript, cddApiSample.InputType.NoInput, cddApiSample.OutputType.NoOutput, false, "Opens the Develop's Guide.", devGuideUrl, function(){eval(devGuideUrl);});
  cddApiSample.registerApi("Help", "API Reference", "Open API Reference", cddApiSample.CallType.JavaScript, cddApiSample.InputType.NoInput, cddApiSample.OutputType.NoOutput, false, "Opens the API Reference document.", "window.open('../docs/API%20Reference/API%20Reference Guide.htm')", function(){ return window.open('../docs/API%20Reference/API%20Reference Guide.htm'); });
};

cddApiSample.attachApiDesc = cddApiSample.newApi("$", "Attach a ChemDraw instance to a DOM node", "attach", cddApiSample.CallType.Async, cddApiSample.InputType.Options, cddApiSample.OutputType.NoOutput, false, "Creates a new ChemDraw instance and attaches it to the specified DOM node. The attach function may be used to attach several instances of ChemDraw to the DOM and may be called at any time. The ChemDraw instance cannot be used immediately once this functions returns. A callback function can be specified in the input JSON object. Then it can be called after the instance becomes available. Please refer to the Developers Guide for details.");
cddApiSample.attachApiDesc.usage = "chemdraw.attach(options);";
