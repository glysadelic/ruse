var mainModule = angular.module('CddWelcomeApp', []);

mainModule.controller('CddWelcomeAppController', function ($scope, $http) {
  $scope.pendingConfig = {};
  $scope.pendingConfig.featureStatuses = [];
  $scope.pendingConfig.toolsOrientations = [];
  $scope.pendingConfig.collapses = [];
  $scope.pendingConfig.stylesheets = [];
  $scope.pendingConfig.groupTools = [];
  $scope.pendingConfig.selectedToolOrientation = "Horizontal";
  $scope.pendingConfig.selectedStyleSheet = "ACS Document 1996";
  $scope.pendingConfig.serviceUrl = "";

  $scope.availableApis = cddApiSample.apiLists;
  $scope.calledApis = [];
  $scope.cddDocuments = [];
  $scope.activeDocId = "";
  $scope.pendingDocId = "";
  $scope.showApiInfo = false;
  $scope.lastDocId = "";
  $scope.defaultViewOnly = false;
  $scope.topDocZIndex = 1;

  for (index in layoutToolsOrder) {
    var tool = layoutToolsOrder[index];
    if (tool.group) {
      var collapse = "auto";
      if (tool.collapse) {
        collapse = tool.collapse;
      }

      $scope.pendingConfig.groupTools.push({group: tool.group, collapse: collapse});
    }
  }

  $scope.postInitActions = function () {
    $scope.newDocument();
    $scope.$apply();
  };

  $scope.showGetStarted = function () {
    $("#getStartedPanel").fadeIn();
  };

  $scope.toggleApiInfoPanel = function () {
    $scope.showApiInfo = !$scope.showApiInfo;
    $scope.resetWorkareaPos();
  };

  $scope.resetWorkareaPos = function () {
    // The width of the apiInfoPanel is defined in the CSS. The right pos of workarea = apiPanel width + 20px.
    var workAreaRightPos = !$scope.showApiInfo ? "0px" : "295px";
    $("#workarea").css("right", workAreaRightPos);
  };

  $scope.newDocument = function (preservePageInfo) {
    if (preservePageInfo == undefined) {
      preservePageInfo = true;
    }

    $scope.resetWorkareaPos();

    var newDocId = "cddDoc_" + new Date().getTime();
    var moveHandleId = newDocId + "_moveHandle";
    var editHandleId = newDocId + "_editHandle";
    var stateHandleId = newDocId + "_stateHandle";
    var resizeHandleId = newDocId + "_resizeHandle";
    var trashHandleId = newDocId + "_trashHandle";

    $("#workarea").append("<div class='document activeDocument' id='" + newDocId + "'>"
      + "<div class='docMoveHandle' id='" + moveHandleId + "'><span class='glyphicon glyphicon-move' aria-hidden='true'></span></div>"
      + "<div class='docEditHandle' id='" + editHandleId + "'><span class='glyphicon glyphicon-edit' aria-hidden='true'></span></div>"
      + "<div class='docStateHandle' id='" + stateHandleId + "'><span class='glyphicon' aria-hidden='true'></span></div>"
      + "<div class='docTrashHandle' id='" + trashHandleId + "'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></div>"
      + "<div class='docResizeHandle' id='" + resizeHandleId + "'></div>"
      + "<div style='width: 100%; height: 100%; overflow: auto;' id='" + newDocId + "_cdd'></div>");

    $("#" + newDocId).resizable().draggable({ handle: "#" + moveHandleId});
    $("#" + moveHandleId).on("click", { targetDocId: newDocId }, $scope.moveDocEventHandler);
    $("#" + editHandleId).on("click", { targetDocId: newDocId }, $scope.editDocEventHandler);
    $("#" + trashHandleId).on("click", { targetDocId: newDocId }, $scope.trashDocEventHandler);

    $scope.pendingDocId = newDocId;

    var callingApi = cddApiSample.attachApiDesc.clone();
    callingApi.showDetails = false;

    $scope.calledApis.unshift(callingApi);
    $scope.initialDocument($scope.pendingDocId);

    var configJsonString = $.parseJSON($scope.generateConfigString());
    var configObject = {
      id: $scope.pendingDocId + "_cdd",
      licenseUrl: './ChemDraw-JS-License.xml',
      preservePageInfo: preservePageInfo,
      viewonly: $scope.defaultViewOnly,
      config: configJsonString,
      callback: $scope.documentAdded,
      errorCallback: function (error) {
        cddApiSample.errorCallback("#" + configObject.id, configObject, error);
      }
    };

    perkinelmer.ChemdrawWebManager.attach(configObject);
  };

  $scope.setActiveDocumentEditable = function (editable) {
    $scope.callApiByName("setViewOnly", editable ? "false" : "true");
    var doc = $scope.findDocument($scope.activeDocId);
    doc.viewOnly = !editable;
  };

  $scope.documentAdded = function (drawingTool) {
    // Add any post initialization here
    $scope.cddDocuments.push({ id: $scope.pendingDocId, tool: drawingTool, thumbnailSrc: "", viewOnly: $scope.defaultViewOnly });
    $("#" + $scope.pendingDocId).chemdraw = drawingTool;
    $scope.maxmizeDocument($scope.pendingDocId, true);
    $scope.pendingDocId = "";
  };

  $scope.listDocuments = function () {
    $scope.saveActiveDocument();
    $('#docsPanel').modal();
  };

  $scope.findDocument = function (docId) {
    for (var docIndex in $scope.cddDocuments) {
      if ($scope.cddDocuments[docIndex].id == docId) {
        return $scope.cddDocuments[docIndex];
      }
    }
  };

  $scope.activateDocument = function (docId, forEdit) {
    if (docId.length == 0) {
      alert("Application Error: docId.length == 0 in activeDocument");
      return;
    }

    var doc = $("#" + docId);
    doc.addClass("activeDocument");
    doc.css("z-index", $scope.topDocZIndex++);

    $scope.activeDocId = docId;

    if (forEdit) {
      $scope.setActiveDocumentEditable(true);
      $scope.callApiByName("fitToContainer");
    }

    // associate with the global drawingtool variable
    var doc = $scope.findDocument($scope.activeDocId);
    if (doc && doc.tool) {
      window.drawingtool = doc.tool;
    }
  };

  $scope.moveDocEventHandler = function (event) {
    $scope.activateDocument(event.data.targetDocId);
  };

  $scope.trashDocEventHandler = function (event) {
    $scope.trashDocument(event.data.targetDocId);
  };

  $scope.trashDocument = function (docId) {
    var docIdToDel = -1;
    for (var i = 0; i < $scope.cddDocuments.length; i++) {
      if ($scope.cddDocuments[i].id == docId) {
        docIdToDel = i;
        break;
      }
    }

    if (docIdToDel >= 0) {
      // If the document to be deleted is the active doc, the document should be disabled to listen to events
      if ($scope.activeDocId == docId) {
        $scope.setActiveDocumentEditable(false);
        $scope.activeDocId = "";
      }

      // Remove the live documents array.
      var removedDocs = $scope.cddDocuments.splice(docIdToDel, 1);
      if (removedDocs.length != 1) {
        alert("Application Error: Impossible. removedDocs.length != 1");
        return;
      }

      // Remove the DOM node of this document.
      removedDocs[0].tool.dispose();
      $("#" + docId).remove();
    } else {
      alert("Application Error: Unable to find the specified doc " + docId);
    }
  };

  $scope.editDocEventHandler = function (event) {
    $scope.editDocument(event.data.targetDocId);
  };

  $scope.editDocument = function (docId) {
    $scope.activateDocument(docId, true);
  };

  $scope.maxDocEventHandler = function (event) {
    $scope.maxmizeDocument(event.data.targetDocId);
  };

  $scope.maxmizeDocument = function (docId, forEdit) {
    var doc = $("#" + docId);
    doc.css("left", "5px");
    doc.css("right", "30px");
    doc.css("bottom", "5px");
    doc.css("top", "5px");
    doc.css("width", "");
    doc.css("height", "");

    var moveHandleId = docId + "_moveHandle";
    $("#" + moveHandleId).css("display", "none");

    var stateHandleId = docId + "_stateHandle";
    var state = $("#" + stateHandleId);
    state.css("display", "block");
    var stateSpan = $("#" + stateHandleId + " > span");
    stateSpan.removeClass("glyphicon-fullscreen");
    stateSpan.addClass("glyphicon-modal-window");
    state.off("click", $scope.maxDocEventHandler);
    state.on("click", { targetDocId: docId }, $scope.restoreDocEventHandler);

    var resizeHandleId = docId + "_resizeHandle";
    $("#" + resizeHandleId).css("display", "none");

    doc.resizable("disable");

    $scope.activateDocument(docId, forEdit);
  };

  $scope.initialDocument = function (docId) {
    var doc = $("#" + docId);
    doc.css("left", "5px");
    doc.css("right", "30px");
    doc.css("bottom", "5px");
    doc.css("top", "5px");
    doc.css("width", "");
    doc.css("height", "");
  };

  $scope.restoreDocEventHandler = function (event) {
    $scope.restoreDocument(event.data.targetDocId);
  };

  $scope.updateFragmentInfoHandler = function (id, image, width, height, x, y, cdxml) {
    return function () {
      $('li[id^="fragment_"]').removeClass('active');
      $('#fragment_' + id).addClass('active');
      $('#modalApiResultContentImg').attr("src", image);
      $('#modalApiResultContentWidth').val(String(width));
      $('#modalApiResultContentHeight').val(String(height));
      $('#modalApiResultContentX').val(String(x));
      $('#modalApiResultContentY').val(String(y));
      $('#modalApiResultContentText').html($("<div/>").text(cdxml).html());
    }
  }

  $scope.restoreDocument = function (docId) {
    $scope.activateDocument(docId);
    var doc = $("#" + docId);
    doc.css("left", "100px");
    doc.css("right", "100px");
    doc.css("bottom", "50px");
    doc.css("top", "100px");

    var moveHandleId = docId + "_moveHandle";
    $("#" + moveHandleId).css("display", "block");

    var stateHandleId = docId + "_stateHandle";
    var state = $("#" + stateHandleId);
    state.css("display", "block");
    var stateSpan = $("#" + stateHandleId + " > span");
    stateSpan.removeClass("glyphicon-modal-window");
    stateSpan.addClass("glyphicon-fullscreen");
    state.off("click", $scope.restoreDocEventHandler);
    state.on("click", { targetDocId: docId }, $scope.maxDocEventHandler);

    var resizeHandleId = docId + "_resizeHandle";
    $("#" + resizeHandleId).css("display", "block");

    doc.resizable("enable");

  };

  $scope.saveActiveDocument = function () {
    var doc = $scope.findDocument($scope.activeDocId);
    if (doc) {
      doc.thumbnailSrc = doc.tool.getImgUrl();
    }
  };

  $scope.closeActiveDoc = function () {
    if ($scope.activeDocId.length == 0) {
      return;
    }

    $scope.saveActiveDocument();
    var doc = $scope.findDocument($scope.activeDocId);
    if (doc) {
      doc.tool.setViewOnly(true);
    }

    $("#" + $scope.activeDocId).animate({
      opacity: 0,
      top: 0,
      right: 0,
      width: "hide"
    }, 1000, function () { });

    $scope.lastDocId = $scope.activeDocId;
    $scope.activeDocId = "";
  };

  $scope.configSwitchTo = function (panelId) {
    $("#configPanel .nav li").removeClass("active");
    $("#configPanel .panel-body .panel").hide();

    $("#" + panelId + "Tab").addClass("active");
    $("#" + panelId).show();
  };

  $scope.generateConfig = function () {
    var configJsonString = $scope.generateConfigString();
    $scope.showResult("Generated Configuration", configJsonString);
    $scope.closeConfig();
  };

  $scope.generateConfigString = function () {
    var result = {};

    result.layout = $scope.generateConfigLayoutString();
    result.features = $scope.generateConfigFeaturesString();

    if ($scope.pendingConfig.serviceUrl && $scope.pendingConfig.serviceUrl.length > 0) {
      result.properties = $scope.generateConfigPropertiesString();
    }

    return JSON.stringify(result, null, '  ');
  };

  $scope.generateConfigFeaturesString = function () {
    var result = {};
    result.enabled = [];
    result.disabled = [];

    for (var i = 0; i < $scope.pendingConfig.featureStatuses.length; i++) {
      if ($scope.pendingConfig.featureStatuses[i].enabled) {
        result.enabled.push($scope.pendingConfig.featureStatuses[i].name);
      } else {
        result.disabled.push($scope.pendingConfig.featureStatuses[i].name);
      }
    }

    return result;
  };

  $scope.generateConfigPropertiesString = function () {
    var result = {"StyleSheet": "New Document"};
    var stylesheet = $scope.pendingConfig.selectedStyleSheet;
    result.chemservice = $scope.pendingConfig.serviceUrl;

    if (stylesheet != "ACS Document 1996" && stylesheet !="New Document") {
      alert("Unsupported stylesheet! " + stylesheet);
      return result;
    }

    result.StyleSheet = stylesheet;
    return result;
  };

  $scope.generateConfigLayoutString = function () {
    var result = { "orientation": "Horizontal", tools: [] };
    var orientation = $scope.pendingConfig.selectedToolOrientation;
    var groupTools = [];

    for (index in $scope.pendingConfig.groupTools) {
      var tool = $scope.pendingConfig.groupTools[index];
      groupTools[tool.group] = tool.collapse;
    }

    if (orientation != "Horizontal" && orientation != "Vertical") {
      alert("Unsupported orientation! " + orientation);
      return result;
    }

    result.orientation = orientation;
    var orderConfig = { "order": [] };
    orderConfig.order = layoutToolsOrder;

    for (index in orderConfig.order) {
      var tool = orderConfig.order[index];
      if(tool.group) {
        var group = tool.group;
        if(groupTools[group]) {
          var collapse = groupTools[group];

          if (collapse != "auto" && collapse != "always" && collapse != "never") {
            alert("Unsupported collapse! " + collapse);
            continue;
          }

          tool.collapse = groupTools[group];
        }
      }
    }


    result.tools.push(orderConfig);
    return result;
  };

  $scope.applyConfig = function () {
    $scope.callApiByName("loadConfig", "$.parseJSON($scope.generateConfigString())");
  };

  $scope.closeConfig = function () {
    $("#configPanel").fadeOut();
    $scope.setActiveDocumentEditable(true);
  };

  $scope.showConfig = function () {
    $scope.setActiveDocumentEditable(false);

    $scope.pendingConfig = {};
    $scope.pendingConfig.featureStatuses = [];
    $scope.pendingConfig.toolsOrientations = [];
    $scope.pendingConfig.collapses = [];
    $scope.pendingConfig.stylesheets = [];
    $scope.pendingConfig.groupTools = [];

    if (!window.preferredConfiguration) {
      for (var i = 0; i < featureNames.length; i++) {
        $scope.pendingConfig.featureStatuses.push({ "name": featureNames[i], "enabled": true });
      }

      $scope.pendingConfig.selectedToolOrientation = "Horizontal";
      $scope.pendingConfig.selectedCollapse = "auto";
      $scope.pendingConfig.serviceUrl = "";
    } else {
      for (var i = 0; i < window.preferredConfiguration.features.enabled.length; i++) {
        $scope.pendingConfig.featureStatuses.push({ "name": window.preferredConfiguration.features.enabled[i], "enabled": true });
      }

      for (var i = 0; i < window.preferredConfiguration.features.disabled.length; i++) {
        $scope.pendingConfig.featureStatuses.push({ "name": window.preferredConfiguration.features.disabled[i], "enabled": false });
      }

      $scope.pendingConfig.selectedToolOrientation = window.preferredConfiguration.layout.orientation;
      $scope.pendingConfig.selectedCollapse = "auto";
      $scope.pendingConfig.selectedStyleSheet = "ACS Document 1996";
      
      $scope.pendingConfig.serviceUrl = window.preferredConfiguration.properties.chemservice;
    }

    for (var i = 0; i < supportedOrientations.length; i++) {
      $scope.pendingConfig.toolsOrientations.push(supportedOrientations[i]);
    }

    for (var i = 0; i < supportedCollapse.length; i++) {
      $scope.pendingConfig.collapses.push(supportedCollapse[i]);
    }

    for (var i = 0; i < supportedStyleSheets.length; i++) {
      $scope.pendingConfig.stylesheets.push(supportedStyleSheets[i]);
    }

    for (index in layoutToolsOrder) {
      var tool = layoutToolsOrder[index];
      if (tool.group) {
        var collapse = "auto";
        if (tool.collapse) {
          collapse = tool.collapse;
        }

        $scope.pendingConfig.groupTools.push({group: tool.group, collapse: collapse});
      }
    }

    $("#configPanel").fadeIn();
  };

  $scope.acceptLicense = function () {
    $('#modalLicense').modal('hide');
    location.reload();
  };

  $scope.acceptInput = function () {
    $("#modalApiInputResult").text("ok");
    $('#modalApiInput').modal("hide");
  };

  $scope.showResult = function (title, content) {
    $('#modalApiResultTitle').html(title);
    if (content instanceof Array) {
      if (title.indexOf('Fragments') > 0) {
        var buttons = '<label>id</label><ul class="nav nav-pills">';
        for (index in content) {
          var fragment = content[index];
          var id = fragment.id;
          buttons += '<li role="presentation" id="fragment_' + id + '"><a href="#">' + id + '</a></li>'
        }

        buttons += '</ul>';

        var imageContent = "<div class='form-group'><label>image</label><div class='well well-sm'><img id='modalApiResultContentImg' src='' style='max-width:100%; max-height:200px;background: #FFF;'/></div></div>";

        var sizeContext = "<div class='form-group'><label for='modalApiResultContentWidth'>width</label><input type='text' class='form-control' id='modalApiResultContentWidth' readonly='true'></div><div class='form-group'><label for='modalApiResultContentHeight'>height</label><input type='text' class='form-control' id='modalApiResultContentHeight' readonly='true'></div>";

        var locationContext = "<div class='form-group'><label for='modalApiResultContentX'>x</label><input type='text' class='form-control' id='modalApiResultContentX' readonly='true'></div><div class='form-group'><label for='modalApiResultContentY'>y</label><input type='text' class='form-control' id='modalApiResultContentY' readonly='true'></div>";

        var cdxmlContent = "<div class='form-group'><label>cdxml</label><textarea id='modalApiResultContentText' class='form-control' rows='10' readonly='true' style='width:100%;resize: none;'>" + $("<div/>").text("").html() + "</textarea></div>";

        var resultContent = '<div>' + buttons + sizeContext + locationContext + imageContent + cdxmlContent+  '</div>';
        $('#modalApiResultContent').html(resultContent);

        for (index in content) {
          var fragment = content[index];
          var id = fragment.id;
          var image = fragment.image;
          var width = fragment.width;
          var height = fragment.height;
          var x = fragment.x;
          var y = fragment.y;
          var cdxml = fragment.cdxml;

          $('#fragment_' + id).on("click", $scope.updateFragmentInfoHandler(id, image, width, height, x, y, cdxml));
        }
      } else {
        var textContent = '';
        content.forEach(function (value) {
          textContent += value + '\r\n';
        });

        var resultContent = "<textarea id='modalApiResultContentText' class='form-control' rows='20' readonly='true' style='width:100%;resize: none;'>" + $("<div/>").text(textContent).html() + "</textarea>";
        $('#modalApiResultContent').html(resultContent);
      }
    } else if ((typeof content) === 'object') {
      if (title.indexOf('Canvas Size') > 0) {
        var resultContent = "<div id='modalApiResultContentText'><div class='form-group'><label for='modalApiResultContentWidth'>width</label> <input type='text' class='form-control' id='modalApiResultContentWidth' readonly='true' value = '" + content.width + "'></div><div class='form-group'><label for='modalApiResultContentHeight'>height</label><input type='text' class='form-control' id='modalApiResultContentHeight' readonly='true' value = '" + content.height + "'></div></div>";
        $('#modalApiResultContent').html(resultContent);
      } else {
        var reader = new FileReader();
        reader.onload = function (event) {
          var base64 = event.target.result;
          var resultContent = "<textarea id='modalApiResultContentText' class='form-control' rows='20' readonly='true' style='width:100%;resize: none;'>" + $("<div/>").text(base64).html() + "</textarea>";
          $('#modalApiResultContent').html(resultContent);
        };

        reader.readAsDataURL(content);
      }
    } else if ((typeof content) === 'number') {
      var resultContent = "<textarea id='modalApiResultContentText' class='form-control' rows='20' readonly='true' style='width:100%;resize: none;'>" + $("<div/>").text(content).html() + "</textarea>";
      $('#modalApiResultContent').html(resultContent);
    } else {
      var resultContent = "<textarea id='modalApiResultContentText' class='form-control' rows='20' readonly='true' style='width:100%;resize: none;'></textarea>";
      if (content.indexOf("data:image/png") == 0) {
        // Show a PNG image in a well
        resultContent = "<div class='well well-sm api-result-image'><img src='" + content + "' /></div>" + resultContent;
      } else if (content.indexOf("</svg>") != -1) {
        // Show an SVG image in a well
        resultContent = "<div class='well well-sm api-result-image'>" + content + "</div>" + resultContent;
      }

      $('#modalApiResultContent').html(resultContent);
      document.getElementById("modalApiResultContentText").innerHTML = $("<div/>").text(content).html();
    }

    $('#modalApiResult').modal().show(function () {
      var btn = document.getElementById('btnCopyResultText');
      var clipboard = new Clipboard(btn);
    });
  };

  $scope.test = function () {
    $scope.newDocument();
  };

  $scope.callApiByName = function (apiName, argsString) {

    for (categoryId in $scope.availableApis) {
      for (apiId in $scope.availableApis[categoryId].apiList) {
        if ($scope.availableApis[categoryId].apiList[apiId].functionName == apiName) {
          return $scope.callApi($scope.availableApis[categoryId].apiList[apiId], argsString);
        }
      }
    }

    alert("Application Error: Unable to find the specified API " + apiName);
  };

  $scope.executeApiCall = function (chemdraw, apiDesc, argsString) {
    // Add the calling API to the model for displaying
    var callingApi = apiDesc.clone();
    callingApi.showDetails = false;
    $scope.calledApis.unshift(callingApi);

    var callResult = undefined;
    try {
      // Execute the API.
      if (apiDesc.executeFunc) {
        callResult = apiDesc.executeFunc(chemdraw, eval(argsString));
      } else {
        // Prepare the API call
        var callString = "alert('Application Error: Invalid JS script to eval')";
        if (apiDesc.callType == cddApiSample.CallType.Async) {
          workingIndicator.show(apiDesc.functionName + "...");
          if (argsString) {
            callString = "chemdraw." + apiDesc.functionName + "(" + argsString + ", function(result, error) {if (error) {alert(error);} else if (apiDesc.outputType !== cddApiSample.OutputType.NoOutput && result){$scope.showResult(apiDesc.displayName, result);}workingIndicator.hide();})";
          } else {
            callString = "chemdraw." + apiDesc.functionName + "(function(result, error) {if (error) {alert(error);} else if (apiDesc.outputType !== cddApiSample.OutputType.NoOutput && result){$scope.showResult(apiDesc.displayName, result);}workingIndicator.hide();})";
          }
        } else if (apiDesc.callType == cddApiSample.CallType.Sync) {
          if (argsString) {
            callString = "chemdraw." + apiDesc.functionName + "(" + argsString + ")";
          } else {
            callString = "chemdraw." + apiDesc.functionName + "()";
          }
        }

        callResult = eval(callString);
      }
    } catch (e) {
      workingIndicator.hide();
      alert(e);
      return;
    }
    if (callResult != undefined && apiDesc.outputType != cddApiSample.OutputType.NoOutput && apiDesc.outputType != cddApiSample.OutputType.Boolean) {
      // This always show the sync call result.
      $scope.showResult(apiDesc.displayName, callResult);
    } else if (apiDesc.outputType == cddApiSample.OutputType.Boolean) {
      $scope.showResult(apiDesc.displayName, callResult.toString());
    }

    if (apiDesc.functionName.indexOf("loadConfig") == 0) {
      $scope.setActiveDocumentEditable(true);
    }

    if (apiDesc.functionName == "loadConfig") {
      var newConfigObject = undefined;
      try {
        newConfigObject = eval(argsString);
        window.preferredConfiguration = newConfigObject;
      } catch (e) {
        workingIndicator.hide();
        alert(e);
        return;
      }
    }

    if (apiDesc.functionName == "loadConfigFromUrl") {
      var newConfigUrl = undefined;
      try {
        newConfigUrl = eval(argsString);
        $.get(newConfigUrl, function(data) {
          window.preferredConfiguration = data;
        });
      } catch (e) {
        workingIndicator.hide();
        alert(e);
        return;
      }
    }
  };

  $scope.callApi = function (apiDesc, argsString) {
    if (apiDesc.requireActiveDoc && $scope.activeDocId.length == 0) {
      alert("User Error: Require Active Doc");
      return;
    }

    // Make sure the API call has valid ChemDraw Direct instance associated.
    var chemdraw = {};
    if (apiDesc.requireActiveDoc) {
      var doc = $scope.findDocument($scope.activeDocId);
      if (!doc) {
        alert("Application Error: Unable to find the specified active document for " + apiDesc.functionName);
        return;
      }
      
      chemdraw = doc.tool;
    } else {
      chemdraw = window.defaultCddInstance;
    }

    if (!chemdraw.getVersion) {
      alert("Application Error: No ChemDraw JS Initialized!");
      return;
    }

    // If API needs input, but no argsString is set, prompt user to input
    if (apiDesc.inputType != cddApiSample.InputType.NoInput && apiDesc.inputType != cddApiSample.InputType.Options && !argsString) {
      var resultEvalString = "";
      if (apiDesc.inputType == cddApiSample.InputType.LongText) {
        $("#modalApiInputBody").html("<textarea id='modalApiInputContent' class='form-control' rows='20' style='width:100%;resize: none;'></textarea>");
        resultEvalString = "$('#modalApiInputContent').val()";
      } else if (apiDesc.inputType == cddApiSample.InputType.ShortText) {
        $("#modalApiInputBody").html("<input id='modalApiInputContent' class='form-control' style='width:100%;'/>");
        resultEvalString = "$('#modalApiInputContent').val()";
      } else if (apiDesc.inputType == cddApiSample.InputType.JsonObject) {
        $("#modalApiInputBody").html("<textarea id='modalApiInputContent' class='form-control' rows='20' style='width:100%;resize: none;'></textarea>");
        resultEvalString = "$.parseJSON($('#modalApiInputContent').val())";
      } else if (apiDesc.inputType == cddApiSample.InputType.Boolean) {
        $("#modalApiInputBody").html("<input type='checkbox' id='modalApiInputContent'/>" + apiDesc.functionName);
        resultEvalString = "$('#modalApiInputContent').is(':checked')";
      } else if (apiDesc.inputType == cddApiSample.InputType.ArrayNumber) {
        $("#modalApiInputBody").html("<div class='alert alert-warning' role='alert'>Input type is Array<number>, for example: <b>58, 70, 38</b><br>The fragment Ids can be got by Get All Fragments Info API.</div><textarea id='modalApiInputContent' class='form-control' rows='20' style='width:100%;resize: none;'></textarea>");
        resultEvalString = "$('#modalApiInputContent').val().split(',').map(function(item) {return parseInt(item, 10);})";
      } else if (apiDesc.inputType == cddApiSample.InputType.Size) {
        $("#modalApiInputBody").html("<div><div class='alert alert-warning' role='alert'>If preservePageInfo set as 'false', setCanvasSize will not work.</div><div class='form-group'><label for='modalApiInputContentWidth'>width</label> <input type='number' min='1' class='form-control' id='modalApiInputContentWidth'> </div><div class='form-group'><label for='modalApiInputContentHeight'>height</label> <input type='number' min='1' class='form-control' id='modalApiInputContentHeight'></div></div><div class='checkbox'><label><input type='checkbox' id='modalApiInputContentCheck'> Do Not Crop Drawings</label>");
        resultEvalString = "{width: parseInt($('#modalApiInputContentWidth').val(), 10), height: parseInt($('#modalApiInputContentHeight').val(), 10)}, {doNotCropDrawings: $('#modalApiInputContentCheck').is(':checked')}";
      } else if (apiDesc.inputType == cddApiSample.InputType.Structure) {
         $("#modalApiInputBody").html("<div><div class='form-group'><label for='modalApiInputContent'>content</label><textarea id='modalApiInputContent' class='form-control' rows='20' style='width:100%;resize: none;'></textarea></div><div class='form-group'><label for='modalApiInputContentMimeType'>mimeType</label><input id='modalApiInputContentMimeType' class='form-control' style='width:100%;'/></div></div><div class='alert alert-warning' role='alert'><b>mimeType:</b> the string format type, ex: chemical/x-cdx, chemical/x-mdl-molfile, chemical/x-helm</div>");
        resultEvalString = "$('#modalApiInputContent').val(), $('#modalApiInputContentMimeType').val()";
      } else if (apiDesc.inputType == cddApiSample.InputType.Handler) {
        resultEvalString = "function (event) {\r\n  console.log(event);\r\n}";
        $("#modalApiInputBody").html("<textarea class='form-control' rows='20' style='width:100%; resize: none;' readonly='true'>" + resultEvalString + "</textarea>");
      } else if (apiDesc.inputType == cddApiSample.InputType.NoArgumentsHandler) {
        resultEvalString = "function () {\r\n  console.log(\""+ apiDesc.functionName +"\");\r\n}";
        $("#modalApiInputBody").html("<textarea class='form-control' rows='20' style='width:100%; resize: none;' readonly='true'>" + resultEvalString + "</textarea>");
      }

      var modalApiInput = $('#modalApiInput');
      modalApiInput.off('hidden.bs.modal');
      modalApiInput.on('hidden.bs.modal', function (e) {
        if ($("#modalApiInputResult").text() == "ok") {
          $scope.executeApiCall(chemdraw, apiDesc, resultEvalString);
        }
      });

      $("#modalApiInputTitle").html(apiDesc.displayName);
      $("#modalApiInputResult").text("");
      $("#modalApiInputContent").val("");
      modalApiInput.modal();
      return;
    }

    $scope.executeApiCall(chemdraw, apiDesc, argsString);
  };

  registerPostInitAction($scope.postInitActions);
});
