#!/bin/bash

. config.txt

if [ ! -f "ruse/oe_license.txt" ]
then
    echo "Copy oe_license.txt to the ruse directory"
    exit 1
fi

if [[ -z ${RUSE_PUBLIC_ADDRESS} ]]
then
     echo "Error: RUSE_PUBLIC_ADDRESS is not defined.  Exiting."
     exit 1
 fi

echo "removing any running containers"
ruse_containers=$(docker ps -a|egrep 'ruse_server|svcrunner'|awk '{print $1}')
for container in $ruse_containers
do
    docker rm -f $container
done

# build/update image
#echo "Building/pulling image"
#docker pull glysade/ruse:$TAG

# start ruse server
if ${RUSESERVER} ; then
    echo "Starting ruse server"
    docker run --name ruse_server -d -it --net=host \
        -m ${RUSE_SERVER_MEMORY} --cpus=${RUSE_SERVER_CPUS} --restart ${RESTART_POLICY} ${RUSE_SERVER_EXTRA_ARGS} \
        -v ${RUSE_DATA}:/ruse glysade/ruse:$TAG -u ${RUN_AS_UID} /bin/sh scripts/ruse.sh -o ${RUSE_PORT}
fi

if ${RUSE3SERVER} ; then
    echo "Starting ruse server"
    docker run --name ruse_server -d -it --net=host \
       --privileged=true \
       --restart ${RESTART_POLICY} ${RUSE3_SERVER_EXTRA_ARGS} \
        -v ${RUSE_DATA}:/ruse glysade/ruse:$TAG -u ${RUN_AS_UID} \
        /bin/sh scripts/ruse.sh -o ${RUSE_PORT} --ruse3 true
fi

if ${DEFAULT_SVCRUNNER} ; then
# start default and fastrocs svcrunners
    echo "Starting default queue container"
    docker run --net=host -v ${RUSE_DATA}:/ruse -m ${SVCRUNNER_MEMORY} \
        --cpus=${SVCRUNNER_CPUS} --name svcrunner_default -d -it --restart ${RESTART_POLICY} ${SVCRUNNER_EXTRA_ARGS}\
        glysade/ruse:${TAG} -u ${RUN_AS_UID} \
        /bin/sh scripts/svcrunner.sh -a ${RUSE_ADDRESS} -e ${ENTREZ_EMAIL} -p ${RUSE_PUBLIC_ADDRESS} -w ${SVCRUNNER_CPUS}\
             -o ${RUSE_PORT} -l ${SVCRUNNER_LOGPATH} -b ${PIPELINE_PILOT_SERVER}
fi

if ${FASTROCS_SVCRUNNER} ; then
    echo "Starting fastrocs queue container"
    docker run --net=host -v ${RUSE_DATA}:/ruse --runtime=nvidia --name svcrunner_fastrocs \
        -m ${FASTROCS_MEMORY} --cpus=${FASTROCS_CPUS} --restart ${RESTART_POLICY} ${FASTROCS_EXTRA_ARGS}\
         -d -it glysade/ruse:${TAG} -u ${RUN_AS_UID} \
        /bin/sh scripts/svcrunner.sh -a ${RUSE_ADDRESS} -e ${ENTREZ_EMAIL} -s "FastROCS Database Search" \
        -q fastrocs -r ${FASTROCS_RESOURCES} -p ${RUSE_PUBLIC_ADDRESS} -w ${FASTROCS_CPUS} -o ${RUSE_PORT} -l ${FASTROCS_LOGPATH}
fi

if ${SSS_SVCRUNNER} ; then
    echo "Starting sss queue container"
    docker run --net=host -v ${RUSE_DATA}:/ruse --name svcrunner_sss -d -it \
        -m ${SSS_MEMORY} --cpus=${SSS_CPUS} --restart ${RESTART_POLICY} ${SSS_EXTRA_ARGS} glysade/ruse:${TAG} -u ${RUN_AS_UID} \
        /bin/sh scripts/svcrunner.sh -a ${RUSE_ADDRESS} -e ${ENTREZ_EMAIL} -s "Substructure Search" -q sss -r ${SSS_RESOURCES} \
        -p ${RUSE_PUBLIC_ADDRESS} -w ${SSS_CPUS} -o ${RUSE_PORT} -l ${SSS_LOGPATH}
fi

if ${RDKIT_SSS_SVCRUNNER} ; then
    echo "Starting RDKit SSS queue container"
    docker run --net=host -v ${RUSE_DATA}:/ruse --name svcrunner_rdkit_sss -d -it \
        -m ${RDKIT_SSS_MEMORY} --cpus=${RDKIT_SSS_CPUS} --restart ${RESTART_POLICY} ${RDKIT_SSS_EXTRA_ARGS} glysade/ruse:${TAG} -u ${RUN_AS_UID} \
        /bin/sh scripts/svcrunner.sh -a ${RUSE_ADDRESS} -e ${ENTREZ_EMAIL} -s "RDKit Substructure Search" -q rdkit_sss -r ${RDKIT_SSS_RESOURCES} \
        -p ${RUSE_PUBLIC_ADDRESS} -w ${RDKIT_SSS_CPUS} -o ${RUSE_PORT} -l ${RDKIT_SSS_LOGPATH}
fi

echo "Running containers"
docker ps

