#!/bin/bash

. config.txt

echo "docker stop ruse_server"
docker stop ruse_server

echo "docker rm ruse_server"
docker rm ruse_server

echo "docker stop svcrunner_default"
docker stop svcrunner_default

echo "docker rm ruse_server"
docker rm svcrunner_default

echo "docker stop svcrunner_default"
docker stop svcrunner_fastrocs

echo "docker rm ruse_server"
docker rm svcrunner_fastrocs

echo "docker stop svcrunner_default"
docker stop svcrunner_sss

echo "docker rm ruse_server"
docker rm svcrunner_sss

echo "docker rmi glysade/ruse"
docker rmi glysade/ruse

docker pull glysade/ruse:$TAG



